package com.kaerman.reading.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ReadingRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadingRegisterApplication.class, args);
	}

}
