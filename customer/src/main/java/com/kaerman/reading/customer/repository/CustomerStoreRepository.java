package com.kaerman.reading.customer.repository;

import com.kaerman.reading.customer.domain.document.CustomerStore;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerStoreRepository extends MongoRepository<CustomerStore, ObjectId> {
}
