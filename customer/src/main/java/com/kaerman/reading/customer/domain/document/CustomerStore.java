package com.kaerman.reading.customer.domain.document;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.LocalDateTime;

@Data
@Builder
public class CustomerStore {

    @Id
    private ObjectId id;

    @Indexed
    private ObjectId customerId;

    private String firstName;

    private String lastName;

    private String username;

    private String password;

    private String salt;

    private String address;

    private String email;

    private LocalDateTime customerCreatedAt;

    private LocalDateTime customerModifiedAt;

    private String reason;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime modifiedAt;

}
