package com.kaerman.reading.customer.domain.dto;

import lombok.Data;

@Data
public class CustomerCreateRequest {

    private String email;
    private String password;
    private String firstname;
    private String lastname;
    private String address;

}
