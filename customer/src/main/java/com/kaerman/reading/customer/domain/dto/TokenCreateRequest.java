package com.kaerman.reading.customer.domain.dto;

import lombok.Data;

@Data
public class TokenCreateRequest {
    private String username;
    private String password;
}
