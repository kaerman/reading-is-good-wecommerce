package com.kaerman.reading.customer.repository;

import com.kaerman.reading.customer.domain.document.Customer;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CustomerRepository extends MongoRepository<Customer, ObjectId> {
    Optional<Customer> findByUsername(String username);
}
