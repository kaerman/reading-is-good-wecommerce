package com.kaerman.reading.customer.service;

import com.kaerman.reading.customer.domain.document.Customer;
import com.kaerman.reading.customer.domain.document.CustomerStore;
import com.kaerman.reading.customer.domain.dto.CustomerCreateRequest;
import com.kaerman.reading.customer.repository.CustomerRepository;
import com.kaerman.reading.customer.repository.CustomerStoreRepository;
import com.kaerman.reading.customer.util.PasswordUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerStoreRepository customerStoreRepository;

    public Optional<Customer> findByUsername(String username) {
        return customerRepository.findByUsername(username);
    }

    @Transactional
    public Customer save(CustomerCreateRequest customerCreateRequest) {
        Optional<Customer> customerOpt = customerRepository.findByUsername(customerCreateRequest.getEmail());
        if (customerOpt.isPresent())
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Username is exist");
        Customer customer = new Customer();
        customer.setUsername(customerCreateRequest.getEmail());
        customer.setEmail(customerCreateRequest.getEmail());
        customer.setSalt(PasswordUtil.generateDefaultLengthSalt());
        customer.setPassword(PasswordUtil.hashThePlainTextPassword(
                customerCreateRequest.getPassword(),
                customer.getSalt()
                ).orElseThrow(() -> new RuntimeException("Create customer hash salt error")));
        customer.setFirstName(customerCreateRequest.getFirstname());
        customer.setLastName(customerCreateRequest.getLastname());
        customer.setAddress(customerCreateRequest.getAddress());
        return customerRepository.save(customer);
    }

    public void saveCustomerStore(Customer customer) {
        CustomerStore customerStore = CustomerStore.builder()
                .customerId(customer.getId())
                .username(customer.getUsername())
                .email(customer.getEmail())
                .address(customer.getAddress())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .password(customer.getPassword())
                .salt(customer.getSalt())
                .reason("save")
                .build();

        customerStoreRepository.save(customerStore);
    }
}
