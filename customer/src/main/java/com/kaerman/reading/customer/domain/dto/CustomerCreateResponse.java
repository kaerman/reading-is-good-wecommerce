package com.kaerman.reading.customer.domain.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerCreateResponse {

    private String id;
    private String email;
    private String username;
    private String firstname;
    private String lastname;
    private String address;

}
