package com.kaerman.reading.customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
public class ReadingCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadingCustomerApplication.class, args);
	}

}
