package com.kaerman.reading.customer.controller;

import com.kaerman.reading.customer.configuration.security.JwtTokenUtil;
import com.kaerman.reading.customer.domain.document.Customer;
import com.kaerman.reading.customer.domain.dto.*;
import com.kaerman.reading.customer.service.CustomerService;
import com.kaerman.reading.customer.util.PasswordUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.security.SecureRandom;
import java.time.Duration;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {

    public static final int TOKEN_EXPIRE_DURATION_HOURS = 48;
    private final CustomerService customerService;
    private final JwtTokenUtil jwtTokenUtil;
    private final RedisTemplate<String, String> redisTemplate;
    private SecureRandom secureRandom = new SecureRandom();

    @Operation(summary = "Create customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Create customer",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = CustomerCreateResponse.class))})
    })
    @PostMapping(value = "/create", produces = "application/json")
    public DataResponse<CustomerCreateResponse> createCustomer(@RequestBody CustomerCreateRequest customerCreateRequest){
        validateCreateCustomerRequest(customerCreateRequest);

        Customer customer = customerService.save(customerCreateRequest);
        customerService.saveCustomerStore(customer);

        CustomerCreateResponse customerCreateResponse = CustomerCreateResponse.builder()
                .id(customer.getId().toHexString())
                .firstname(customer.getFirstName())
                .lastname(customer.getLastName())
                .address(customer.getAddress())
                .email(customer.getEmail())
                .username(customer.getUsername())
                .build();
        return DataResponse.of(customer.getId().toHexString(), customerCreateResponse);
    }

    private void validateCreateCustomerRequest(CustomerCreateRequest customerCreateRequest) {
        if (StringUtils.isBlank(customerCreateRequest.getEmail())
                || StringUtils.isBlank(customerCreateRequest.getPassword())
                || StringUtils.isBlank(customerCreateRequest.getAddress())
                || StringUtils.isBlank(customerCreateRequest.getFirstname())
                || StringUtils.isBlank(customerCreateRequest.getLastname())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Customer create request is not valid");
        }
    }

    @Operation(summary = "Create token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Create token",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = TokenCreateResponse.class))})
    })
    @PostMapping(value = "/token/create", produces = "application/json")
    public DataResponse<TokenCreateResponse> createToken(@RequestBody TokenCreateRequest tokenCreateRequest){
        validateTokenCreateRequest(tokenCreateRequest);
        Optional<Customer> customerOpt = customerService.findByUsername(tokenCreateRequest.getUsername());
        customerOpt.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));

        Customer customer = customerOpt.get();
        if (PasswordUtil.verifyThePlainTextPassword(
                tokenCreateRequest.getPassword(),
                customer.getPassword(),
                customer.getSalt()))
        {
            String token = storeToken(customer);
            TokenCreateResponse tokenCreateResponse = TokenCreateResponse.builder()
                    .token(token)
                    .build();
            return DataResponse.of(customer.getId().toHexString(), tokenCreateResponse);
        }

        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    private String storeToken(Customer customer) {
        String token;
        String jwtToken = jwtTokenUtil.generateAccessToken(customer.getId().toHexString());
        do {
             token = String.valueOf(secureRandom.nextLong());
        } while (redisTemplate.opsForValue().get(token) != null);
        redisTemplate.opsForValue().set(token, jwtToken);
        redisTemplate.expire(token, Duration.ofHours(TOKEN_EXPIRE_DURATION_HOURS));
        return token;
    }

    private void validateTokenCreateRequest(TokenCreateRequest tokenCreateRequest) {
        if (StringUtils.isBlank(tokenCreateRequest.getUsername())
                || StringUtils.isBlank(tokenCreateRequest.getPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Token create request is not valid");
        }
    }

}
