package com.kaerman.reading.gateway.filter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class RatePreFilter implements GlobalFilter, Ordered {

    private static final String BEARER_STR = "Bearer ";

    private final ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        return checkRate(exchange, chain);
    }

    private Mono<Void> checkRate(ServerWebExchange exchange, GatewayFilterChain chain) {
        final String hostString = exchange.getRequest().getRemoteAddress().getHostString();
        log.warn("Check rate host [{}]", hostString);
        final String key = "Rate_" + hostString;
        return reactiveRedisTemplate.opsForValue().increment(key)
                    .flatMap(requestCount -> {
                        reactiveRedisTemplate.expire(key, Duration.ofSeconds(100)).subscribe();
                        if (requestCount > 10) {
                            log.warn("Global Pre Filter there is too many requests");
                            exchange.getResponse().setStatusCode(HttpStatus.TOO_MANY_REQUESTS);
                            return exchange.getResponse().setComplete();
                        }
                        return chain.filter(exchange);
                   });
    }

    @Override
    public int getOrder() {
        return -1;
    }
}