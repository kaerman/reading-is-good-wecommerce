package com.kaerman.reading.gateway.filter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class AuthenticationPreFilter implements GlobalFilter, Ordered {

    private static final String BEARER_STR = "Bearer ";

    private final ReactiveRedisTemplate<String, String> reactiveRedisTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.warn("Global Pre Filter executed request remote address {}, authentication header {}",
                exchange.getRequest().getRemoteAddress(),
                Strings.join(exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION), ',')
        );

        Mono<Void> authenticationMono = checkAuthentication(exchange, chain);
        if (authenticationMono != null)
            return authenticationMono;
        return chain.filter(exchange);
    }

    private Mono<Void> checkAuthentication(ServerWebExchange exchange, GatewayFilterChain chain) {
        List<String> authToken = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION);
        if (!CollectionUtils.isEmpty(authToken)) {
            String token = authToken.stream().findFirst().get().substring(BEARER_STR.length());
            log.warn("Token [{}]", token);
            return reactiveRedisTemplate.opsForValue().get(token)
                    .flatMap(jwt -> {
                        log.warn("Global Pre Filter there is authorization {}", jwt);
                        ServerHttpRequest modifiedRequest = exchange.getRequest().mutate().
                                header("X-Authorization", jwt).
                                build();
                        return chain.filter(exchange.mutate().request(modifiedRequest).build());
                    })
                    .switchIfEmpty(Mono.defer(() -> {
                        log.warn("Global Pre Filter there is no authorization");
                        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
                        return exchange.getResponse().setComplete();
                    }));
        }
        return null;
    }

    @Override
    public int getOrder() {
        return 0;
    }
}