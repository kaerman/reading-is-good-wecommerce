package com.kaerman.reading.gateway.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.*;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Slf4j
@Configuration
public class RedisConfig {

    @Value("${redis.lettuce.hostname:localhost}")
    private String hostname;

    @Value("${redis.lettuce.port:6379}")
    private int port;

    @Value("${redis.lettuce.username:}")
    private String username;

    @Value("${redis.lettuce.password:}")
    private String password;

    @Value("${redis.lettuce.database:0}")
    private Integer database;

    @Bean
    public RedisStandaloneConfiguration standaloneConfig() {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration();
        if(StringUtils.isNotBlank(username)){
            config.setUsername(username);
        }
        if(StringUtils.isNotBlank(password)){
            config.setPassword(RedisPassword.of(password));
        }
        config.setDatabase(database);
        return config;
    }

    @Bean(name = "reactiveRedisConnectionFactory")
    public ReactiveRedisConnectionFactory reactiveRedisConnectionFactory() {
        return new LettuceConnectionFactory(standaloneConfig());
    }

    @Primary
    @Bean
    public ReactiveRedisTemplate<String, String> reactiveRedisTemplate(@Qualifier("reactiveRedisConnectionFactory") ReactiveRedisConnectionFactory reactiveRedisConnectionFactory) {
        RedisSerializationContext.RedisSerializationContextBuilder<String, String> builder = RedisSerializationContext.newSerializationContext(new StringRedisSerializer());
        return new ReactiveRedisTemplate<>(reactiveRedisConnectionFactory, builder.value(new StringRedisSerializer()).build());
    }
}

