package com.kaerman.reading.order.domain.document;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.data.mongodb.core.mapping.FieldType.DECIMAL128;

@Document
@Data
public class MonthlyOrderStat {

    @Id
    private ObjectId id;

    @Indexed
    private String month;

    private int totalOrder;

    private int totalBook;

    @Field(targetType = DECIMAL128)
    private BigDecimal totalAmount = new BigDecimal(0);
}
