package com.kaerman.reading.order.client.dto;

import lombok.Data;

import java.util.List;

@Data
public class BookStockUpdateRequest {

    private String id;

    @Data
    public static class Item {
        private String isbn;
        private int numberOfChangeInStock;
    }

    private List<Item> items;
}
