package com.kaerman.reading.order.client;

import com.kaerman.reading.order.client.dto.BookStockUpdateRequest;
import com.kaerman.reading.order.client.dto.BookStockUpdateResponse;
import com.kaerman.reading.order.domain.dto.DataResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name="book")
public interface BookClient {
    @PostMapping("/book/stock/update")
    DataResponse<BookStockUpdateResponse> bookStockUpdate(BookStockUpdateRequest bookStockUpdateRequest);
}
