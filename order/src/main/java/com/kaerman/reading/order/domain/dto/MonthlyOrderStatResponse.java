package com.kaerman.reading.order.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class MonthlyOrderStatResponse {

    private String month;
    private int orderCount;
    private int totalBook;
    private BigDecimal totalAmount;

}
