package com.kaerman.reading.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ReadingOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadingOrderApplication.class, args);
	}

}
