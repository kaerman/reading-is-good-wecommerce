package com.kaerman.reading.order.service;

import com.kaerman.reading.order.domain.document.MonthlyOrderStat;
import com.kaerman.reading.order.domain.document.Order;
import com.kaerman.reading.order.repository.MonthlyOrderStatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class StatService {

    private final MonthlyOrderStatRepository monthlyOrderStatRepository;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-LLLL");

    public List<MonthlyOrderStat> getMonthlyOrderStats() {
        return monthlyOrderStatRepository.findAll();
    }

    @Transactional
    public MonthlyOrderStat updateMonthlyOrderStat(Order order) {
        String month = order.getCreatedAt().format(formatter);
        Optional<MonthlyOrderStat> monthlyOrderStatOpt = monthlyOrderStatRepository.findByMonth(month);
        MonthlyOrderStat monthlyOrderStat = monthlyOrderStatOpt.orElse(new MonthlyOrderStat());
        monthlyOrderStat.setMonth(month);
        monthlyOrderStat.setTotalOrder(monthlyOrderStat.getTotalOrder()+1);
        monthlyOrderStat.setTotalBook(monthlyOrderStat.getTotalBook()
                +order.getItems().stream().map(item -> item.getQuantity())
                .reduce(0, Integer::sum));
        monthlyOrderStat.setTotalAmount(monthlyOrderStat.getTotalAmount().add(order.getTotalPrice()));
        return monthlyOrderStatRepository.save(monthlyOrderStat);
    }
}
