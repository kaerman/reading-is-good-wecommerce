package com.kaerman.reading.order.domain.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class GetOrderByDateRequest {
    private LocalDateTime startDate;
    private LocalDateTime endDate;
}
