package com.kaerman.reading.order.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataResponse<T> {
    private String id;
    private int status;
    private T response;

    public static <R> DataResponse<R> of(String id, R response) {
        return new DataResponse<>(id, HttpStatus.OK.value(), response);
    }

    public static <R> DataResponse<R> of(String id, int status, R response) {
        return new DataResponse<>(id, status, response);
    }

    public static <R> DataResponse<R> of(String id, HttpStatus httpStatus, R response) {
        return new DataResponse<>(id, httpStatus.value(), response);
    }
}
