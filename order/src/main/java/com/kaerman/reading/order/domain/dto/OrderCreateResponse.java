package com.kaerman.reading.order.domain.dto;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class OrderCreateResponse {

    @Data
    @Builder
    public static class Item {
        private ObjectId id;
        private String isbn;
        private int quantity;
        private BigDecimal itemPrice;
    }

    private BigDecimal totalPrice;

    private List<OrderCreateResponse.Item> items;
}
