package com.kaerman.reading.order.repository;

import com.kaerman.reading.order.domain.document.Order;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderRepository extends MongoRepository<Order, ObjectId> {
    Page<Order> findByCustomerIdOrderByCreatedAtDesc(ObjectId customerId, Pageable pageable);
    List<Order> findByCreatedAtBetweenOrderByCreatedAtDesc(LocalDateTime startDate, LocalDateTime endDate);
}
