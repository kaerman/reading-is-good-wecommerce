package com.kaerman.reading.order.domain.dto;

import lombok.Data;
import org.bson.types.ObjectId;

import java.util.List;

@Data
public class OrderCreateRequest {
    private String id;

    private ObjectId customerId;

    @Data
    public class Item {
        private String isbn;
        private int quantity;
    }

    private List<OrderCreateRequest.Item> items;
}
