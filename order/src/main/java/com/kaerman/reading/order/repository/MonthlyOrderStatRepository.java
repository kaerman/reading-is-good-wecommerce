package com.kaerman.reading.order.repository;

import com.kaerman.reading.order.domain.document.MonthlyOrderStat;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface MonthlyOrderStatRepository extends MongoRepository<MonthlyOrderStat, ObjectId> {
    Optional<MonthlyOrderStat> findByMonth(String month);
}
