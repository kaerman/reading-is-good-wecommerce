package com.kaerman.reading.order.controller;

import com.kaerman.reading.order.domain.document.MonthlyOrderStat;
import com.kaerman.reading.order.domain.dto.DataResponse;
import com.kaerman.reading.order.service.StatService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/order/stats")
@RequiredArgsConstructor
public class StatisticsController {

    private final StatService statService;
    private SecureRandom secureRandom = new SecureRandom();

    @Operation(summary = "Order statistics")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DataResponse.class))})
    })
    @PostMapping(value = "/monthly", produces = "application/json")
    public DataResponse<List<MonthlyOrderStat>> getMonthlyOrderStats() {
        return DataResponse.of(String.valueOf(secureRandom.nextLong()), statService.getMonthlyOrderStats());
    }
}
