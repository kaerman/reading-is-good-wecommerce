package com.kaerman.reading.order.service;

import com.kaerman.reading.order.client.dto.BookStockUpdateResponse;
import com.kaerman.reading.order.domain.document.Item;
import com.kaerman.reading.order.domain.document.Order;
import com.kaerman.reading.order.domain.dto.GetOrderByDateRequest;
import com.kaerman.reading.order.domain.dto.OrderCreateRequest;
import com.kaerman.reading.order.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class OrderService {

    private final OrderRepository orderRepository;

    public Optional<Order> getOrder(ObjectId orderId) {
        return orderRepository.findById(orderId);
    }

    public Page<Order> getCustomerOrdersCreatedAtDesc(ObjectId customerId, Pageable pageable) {
        return orderRepository.findByCustomerIdOrderByCreatedAtDesc(customerId, pageable);
    }

    public List<Order> getOrdersCreateDateBetween(GetOrderByDateRequest getOrderByDateRequest) {
        return orderRepository.findByCreatedAtBetweenOrderByCreatedAtDesc(getOrderByDateRequest.getStartDate(),
                getOrderByDateRequest.getEndDate());
    }

    @Transactional
    public Order save(ObjectId customerId, BookStockUpdateResponse bookStockUpdate, String status) {
        Order order = new Order();
        order.setCustomerId(customerId);
        order.setItems(createOrderItem(bookStockUpdate.getItems()));
        order.setTotalPrice(order.getItems().stream()
                .map(item -> item.getItemPrice().multiply(BigDecimal.valueOf(item.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add));
        order.setStatus(status);
        return orderRepository.save(order);
    }

    private List<Item> createOrderItem(List<BookStockUpdateResponse.Item> items) {
        return items.stream().map(item -> {
            Item dbItem = new Item();
            dbItem.setId(item.getId());
            dbItem.setIsbn(item.getIsbn());
            dbItem.setQuantity(item.getStock());
            dbItem.setItemPrice(item.getPrice());
            return dbItem;
        }).collect(Collectors.toList());
    }
}
