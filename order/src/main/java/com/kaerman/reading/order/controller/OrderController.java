package com.kaerman.reading.order.controller;

import com.kaerman.reading.order.client.BookClient;
import com.kaerman.reading.order.client.dto.BookStockUpdateRequest;
import com.kaerman.reading.order.client.dto.BookStockUpdateResponse;
import com.kaerman.reading.order.domain.document.Order;
import com.kaerman.reading.order.domain.dto.DataResponse;
import com.kaerman.reading.order.domain.dto.GetOrderByDateRequest;
import com.kaerman.reading.order.domain.dto.OrderCreateRequest;
import com.kaerman.reading.order.domain.dto.OrderCreateResponse;
import com.kaerman.reading.order.service.OrderService;
import com.kaerman.reading.order.service.StatService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final BookClient bookClient;
    private final OrderService orderService;
    private final StatService statService;
    private SecureRandom secureRandom = new SecureRandom();

    @Operation(summary = "Get order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get order",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DataResponse.class))})
    })
    @PostMapping(value = "/{id}", produces = "application/json")
    public DataResponse<Order> getOrder(@PathVariable(name = "id") ObjectId orderId) {
        Optional<Order> orderOpt = orderService.getOrder(orderId);
        Order order = orderOpt.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return DataResponse.of(order.getId().toHexString(), order);
    }

    @Operation(summary = "Get orders")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get order",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DataResponse.class))})
    })
    @PostMapping(value = "/bydate", produces = "application/json")
    public DataResponse<List<Order>> getOrders(@RequestBody GetOrderByDateRequest getOrderByDateRequest) {
        List<Order> orders = orderService.getOrdersCreateDateBetween(getOrderByDateRequest);
        return DataResponse.of(String.valueOf(secureRandom.nextLong()), orders);
    }

    @Operation(summary = "Get customer order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get Customer order",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = DataResponse.class))})
    })
    @PostMapping(value = "/customer/{id}", produces = "application/json")
    public DataResponse<Page<Order>> getCustomerOrders(@PathVariable(name = "id") ObjectId customerId, Pageable pageable) {
        Page<Order> customerOrders = orderService.getCustomerOrdersCreatedAtDesc(customerId, pageable);
        return DataResponse.of(customerId.toHexString(), customerOrders);
    }

    @Operation(summary = "Create order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Create order",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = OrderCreateResponse.class))})
    })
    @PostMapping(value = "/create", produces = "application/json")
    public DataResponse<OrderCreateResponse> createOrder(@RequestBody OrderCreateRequest orderCreateRequest){
        validateOrderCreateRequest(orderCreateRequest);
        BookStockUpdateRequest bookStockUpdateRequest = createBookStockUpdateRequest(orderCreateRequest);
        DataResponse<BookStockUpdateResponse> bookStockUpdateResponse = bookClient.bookStockUpdate(bookStockUpdateRequest);
        if (checkStock(bookStockUpdateResponse)) {
            Order order = orderService.save(orderCreateRequest.getCustomerId(), bookStockUpdateResponse.getResponse(), "Success");
            statService.updateMonthlyOrderStat(order);
            OrderCreateResponse orderCreateResponse = OrderCreateResponse.builder()
                    .totalPrice(order.getTotalPrice())
                    .items(order.getItems().stream()
                            .map(item -> OrderCreateResponse.Item.builder()
                                    .id(item.getId())
                                    .isbn(item.getIsbn())
                                    .itemPrice(item.getItemPrice())
                                    .quantity(item.getQuantity())
                                    .build())
                            .collect(Collectors.toList()))
                    .build();
            return DataResponse.of(orderCreateRequest.getId(), orderCreateResponse);
        } else {
            BookStockUpdateRequest bookStockUpdateRequestRollBack = createBookStockUpdateRequestRollback(bookStockUpdateRequest);
            bookStockUpdateResponse = bookClient.bookStockUpdate(bookStockUpdateRequestRollBack);
            if (bookStockUpdateResponse.getStatus() == HttpStatus.OK.value()) {
                orderService.save(orderCreateRequest.getCustomerId(),
                        bookStockUpdateResponse.getResponse() , "Rollback");
            }
            log.error("Book stock failed {}", orderCreateRequest);
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    private BookStockUpdateRequest createBookStockUpdateRequestRollback(BookStockUpdateRequest bookStockUpdateRequest) {
        for (BookStockUpdateRequest.Item item : bookStockUpdateRequest.getItems()) {
            item.setNumberOfChangeInStock(item.getNumberOfChangeInStock() * -1);
        }
        return bookStockUpdateRequest;
    }

    private boolean checkStock(DataResponse<BookStockUpdateResponse> bookStockUpdateResponse) {
        if (bookStockUpdateResponse.getStatus() != HttpStatus.OK.value()) {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY);
        }
        return bookStockUpdateResponse.getResponse().getItems().stream()
                .anyMatch(item -> item.getStock() < 0);
    }

    private BookStockUpdateRequest createBookStockUpdateRequest(OrderCreateRequest orderCreateRequest) {
        BookStockUpdateRequest bookStockUpdateRequest = new BookStockUpdateRequest();
        bookStockUpdateRequest.setId(String.valueOf(secureRandom.nextLong()));
        List<BookStockUpdateRequest.Item> items = orderCreateRequest.getItems().stream()
                .map(orderItem -> {
                    BookStockUpdateRequest.Item stockItem = new BookStockUpdateRequest.Item();
                    stockItem.setIsbn(orderItem.getIsbn());
                    stockItem.setNumberOfChangeInStock(orderItem.getQuantity());
                    return stockItem;
                }).collect(Collectors.toList());
        bookStockUpdateRequest.setItems(items);
        return bookStockUpdateRequest;
    }

    private void validateOrderCreateRequest(OrderCreateRequest orderCreateRequest) {
        if (StringUtils.isBlank(orderCreateRequest.getId())
                || CollectionUtils.isEmpty(orderCreateRequest.getItems())
                || orderCreateRequest.getItems().stream()
                    .anyMatch(item -> item.getIsbn().isBlank() || item.getQuantity() <= 0)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Order create request is not valid");
        }
    }

}
