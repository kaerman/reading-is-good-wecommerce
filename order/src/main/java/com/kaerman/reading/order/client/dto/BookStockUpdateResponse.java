package com.kaerman.reading.order.client.dto;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class BookStockUpdateResponse {

    @Data
    @Builder
    public static class Item {
        private ObjectId id;
        private String isbn;
        private int stock;
        private BigDecimal price;
    }

    private List<Item> items;
}