package com.kaerman.reading.order.domain.document;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;

import static org.springframework.data.mongodb.core.mapping.FieldType.DECIMAL128;

@Data
public class Item {
    private ObjectId id;
    private String isbn;
    private int quantity;
    @Field(targetType = DECIMAL128)
    private BigDecimal itemPrice;
    @Field(targetType = DECIMAL128)
    private BigDecimal totalPrice;
}
