package com.kaerman.reading.book.domain.dto;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class BookCreateResponse {
    private ObjectId id;
    private String isbn;
    private String name;
    private List<String> authors;
    private BigDecimal price;
}
