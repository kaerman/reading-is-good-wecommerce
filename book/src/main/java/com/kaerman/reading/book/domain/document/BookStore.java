package com.kaerman.reading.book.domain.document;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class BookStore {
    @Id
    private ObjectId id;

    @Indexed
    private ObjectId bookId;

    private String isbn;

    private String name;

    private List<String> authors;

    private BigDecimal price;

    private int stock;

    private LocalDateTime bookCreatedAt;

    private LocalDateTime bookModifiedAt;

    private String reason;

    @CreatedDate
    private LocalDateTime createdAt;

    @LastModifiedDate
    private LocalDateTime modifiedAt;
}
