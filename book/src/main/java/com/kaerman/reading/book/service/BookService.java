package com.kaerman.reading.book.service;

import com.kaerman.reading.book.domain.document.Book;
import com.kaerman.reading.book.domain.document.BookStore;
import com.kaerman.reading.book.domain.dto.BookCreateRequest;
import com.kaerman.reading.book.domain.dto.BookStockUpdateRequest;
import com.kaerman.reading.book.repository.BookRepository;
import com.kaerman.reading.book.repository.BookStoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookService {

    private final BookRepository bookRepository;
    private final BookStoreRepository bookStoreRepository;

    @Transactional
    public Book save(BookCreateRequest bookCreateRequest) {
        Optional<Book> bookOpt = bookRepository.findByIsbn(bookCreateRequest.getIsbn());
        if (bookOpt.isPresent())
            throw new ResponseStatusException(HttpStatus.CONFLICT, "book is exist");
        Book book = new Book();
        book.setIsbn(bookCreateRequest.getIsbn());
        book.setName(bookCreateRequest.getName());
        book.setPrice(bookCreateRequest.getPrice());
        book.setAuthors(bookCreateRequest.getAuthors());
        return bookRepository.save(book);
    }

    public void saveBookStore(Book book, String reason) {
        BookStore bookStore = BookStore.builder()
                .bookId(book.getId())
                .authors(book.getAuthors())
                .isbn(book.getIsbn())
                .name(book.getName())
                .price(book.getPrice())
                .bookCreatedAt(book.getCreatedAt())
                .bookModifiedAt(book.getModifiedAt())
                .reason(reason)
                .build();
        bookStoreRepository.save(bookStore);
    }

    @Transactional
    public List<Book> updateBookStock(BookStockUpdateRequest bookStockUpdateRequest) {
        List<Book> books = new ArrayList<>(bookStockUpdateRequest.getItems().size());
        for (BookStockUpdateRequest.Item item : bookStockUpdateRequest.getItems()) {
            Optional<Book> bookOpt = bookRepository.findByIsbn(item.getIsbn());
            bookOpt.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
            Book book = bookOpt.get();
            book.setStock(book.getStock() + item.getNumberOfChangeInStock());
            bookRepository.save(book);
            books.add(book);
        }
        return books;
    }
}
