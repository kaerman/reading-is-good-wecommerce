package com.kaerman.reading.book.repository;

import com.kaerman.reading.book.domain.document.BookStore;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookStoreRepository extends MongoRepository<BookStore, ObjectId> {
}
