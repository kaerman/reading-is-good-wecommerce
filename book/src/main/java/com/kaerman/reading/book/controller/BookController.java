package com.kaerman.reading.book.controller;

import com.kaerman.reading.book.domain.document.Book;
import com.kaerman.reading.book.domain.dto.*;
import com.kaerman.reading.book.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @Operation(summary = "Create book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Create book",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = BookCreateResponse.class))})
    })
    @PostMapping(value = "/create", produces = "application/json")
    public DataResponse<BookCreateResponse> createBook(@RequestBody BookCreateRequest bookCreateRequest){
        validateBookCreateRequest(bookCreateRequest);
        Book book = bookService.save(bookCreateRequest);
        bookService.saveBookStore(book, "save");
        BookCreateResponse customerCreateResponse = BookCreateResponse.builder()
                .id(book.getId())
                .isbn(book.getIsbn())
                .name(book.getName())
                .authors(book.getAuthors())
                .build();

        return DataResponse.of(book.getId().toHexString(), customerCreateResponse);
    }

    private void validateBookCreateRequest(BookCreateRequest bookCreateRequest) {
        if (StringUtils.isBlank(bookCreateRequest.getIsbn())
                || StringUtils.isBlank(bookCreateRequest.getName())
                || CollectionUtils.isEmpty(bookCreateRequest.getAuthors())
                || bookCreateRequest.getPrice() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Book create request is not valid");
        }
    }

    @Operation(summary = "Update book stock")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update book stock",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = BookCreateResponse.class))})
    })
    @PostMapping(value = "/stock/update", produces = "application/json")
    public DataResponse<BookStockUpdateResponse> updateBookStock(@RequestBody BookStockUpdateRequest bookStockUpdateRequest){
        validateBookStockUpdateRequest(bookStockUpdateRequest);
        List<Book> books = bookService.updateBookStock(bookStockUpdateRequest);
        books.forEach(book -> bookService.saveBookStore(book, "update"));
        List<BookStockUpdateResponse.Item> bookStockUpdateResponseItems = mapBookStockUpdateResponseItem(books);
        BookStockUpdateResponse bookStockUpdateResponse = BookStockUpdateResponse.builder()
                .items(bookStockUpdateResponseItems)
                .build();
        return DataResponse.of(bookStockUpdateRequest.getId(), bookStockUpdateResponse);
    }

    private void validateBookStockUpdateRequest(BookStockUpdateRequest bookStockUpdateRequest) {
        for (BookStockUpdateRequest.Item item : bookStockUpdateRequest.getItems()) {
            if (StringUtils.isBlank(item.getIsbn())
                    || item.getNumberOfChangeInStock() == 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Book stock update request is not valid");
            }
        }
    }

    private List<BookStockUpdateResponse.Item> mapBookStockUpdateResponseItem(List<Book> books) {
        return books.stream().map(book -> BookStockUpdateResponse.Item.builder()
                .id(book.getId())
                .isbn(book.getIsbn())
                .stock(book.getStock())
                .price(book.getPrice())
                .build())
                .collect(Collectors.toList());
    }

}
