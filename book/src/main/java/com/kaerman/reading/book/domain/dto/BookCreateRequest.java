package com.kaerman.reading.book.domain.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class BookCreateRequest {
    private String isbn;
    private String name;
    private List<String> authors;
    private BigDecimal price;
}
