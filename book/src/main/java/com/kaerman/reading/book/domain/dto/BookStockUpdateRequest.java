package com.kaerman.reading.book.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class BookStockUpdateRequest {

    private String id;

    @Data
    public class Item {
        private String isbn;
        private int numberOfChangeInStock;
    }

    private List<Item> items;
}
