package com.kaerman.reading.book.repository;

import com.kaerman.reading.book.domain.document.Book;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface BookRepository extends MongoRepository<Book, ObjectId> {
    Optional<Book> findByIsbn(String isbn);
}
