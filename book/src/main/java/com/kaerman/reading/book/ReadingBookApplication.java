package com.kaerman.reading.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadingBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadingBookApplication.class, args);
	}

}
