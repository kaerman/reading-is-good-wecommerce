# Reading is good Application

ReadingIsGood is an online books retail firm which operates only on the Internet. Main
target of ReadingIsGood is to deliver books from its one centralized warehouse to their
customers within the same day. That is why stock consistency is the first priority for their
vision operations.

### Prerequisites

Java JDK 11 or above and Maven

### Installing
Microservice compilation 

Compile -> ./mvnw compile\
Package -> ./mvnw package\
Install -> ./mvnw install

### Running the tests
Test   ->  ./mvnw test

### Running Reading-is-good
Docker build and run -> docker-compose up

### Swagger for Reading is good application
Swagger link -> http://localhost:8080/swagger-ui-book.html \
Swagger link -> http://localhost:8080/swagger-ui-customer.html \
Swagger link -> http://localhost:8080/swagger-ui-order.html
